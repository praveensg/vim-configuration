#!/bin/bash
PATH="$(pwd)"
#which cp
/bin/mkdir -p ~/.vim/colors
echo "Copying theme from $PATH/colors/ to ~/.vim/colors/"
/bin/cp ./colors/* ~/.vim/colors/
echo "Copying vimrc file to ~/.vimrc"
/bin/cp vimrc ~/.vimrc
echo "Done."
